#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
  char name[80];
  char surname[80];
  double refereeScores[6];
  double average;
} Skater;

double calculateScore(const Skater*);
int loadSkaters(Skater**);
void printSkaters(Skater*, int);

int main(void) {
  Skater* skaters;
  int count=loadSkaters(&skaters);
  //printSkaters(skaters, count);

  int num=0;
  for(int i=0; i<count; i++)
    if(skaters[i].average > skaters[num].average)
      num=i;
  Skater* winner=&skaters[num];
  printf("Winner of the Skating Championship is:\n");
  printf("%s %s with %.2lf points in total.\n\n",winner->name, winner->surname, winner->average);
}


double calculateScore(const Skater* skater){
  int min=skater->refereeScores[0];
  int max=min;
  int sum=0;
  for(int i=0; i<6; i++){
    int score=skater->refereeScores[i];
    sum+=score;
    if(score>max)
      max=score;
    if(score < min)
      min=score;
  }
  sum-=min+max;
  return sum/4.0;
}


int loadSkaters(Skater** skatersPtr){
  char fileName[80];
  printf("Enter the file name to read: ");
  scanf("%s",fileName);
  Skater* skaters;
  FILE *fp = fopen(fileName, "r");
  char name[80];
  int count=0;
  while(fscanf(fp, "%s", name) != EOF){
    if(count==0)
      skaters=malloc(sizeof(Skater));
    else
      skaters=realloc(skaters, sizeof(Skater) * count+1);

    strcpy(skaters[count].name, name);
    fscanf(fp, "%s", skaters[count].surname);
    for(int i=0; i<6; i++)
      fscanf(fp, "%lf", skaters[count].refereeScores + i);
    skaters[count].average=calculateScore(&skaters[count]);

    printSkaters(skaters + count, 1);
    count++;
  }
  fclose(fp);
  printf("Loading complete.\nFile closed.\n\n");
  *skatersPtr=skaters;
  return count;
}

void printSkaters(Skater* skaters, int count){
  for(int i=0; i<count;i++){
    printf("==Skater #%d: %s %s==\nScores: ", i+1, skaters[i].name, skaters[i].surname);
    for(int n=0; n<6; n++)
      printf("%.lf ", skaters[i].refereeScores[n]);
    printf("\nAverage point is %.2lf\n\n", skaters[i].average);
  }
}
