#define FILENAME "employee.bin"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
void menu();
void convertTextFile(FILE*);
void addRecord(FILE*);
void deleteRecord(FILE*);
void updateRecord(FILE*);
void showRecords(FILE*);

int main() {
  printf("\n");
  while(1){
    menu();
  }
  return 0;
}


typedef struct {
  int id;
  char name[20];
  char dept[20];
  int birthYear;
  int salary;
} employeeData;
void printEmployeeColumns(FILE*);
void printEmployee(FILE*, employeeData);

void menu(){
  if( access(FILENAME, F_OK ) == -1 ) {
    printf("ERROR: File %s doesn't exist, closing.\n", FILENAME);
    exit(1);
  }
  printf("EMPLOYEE RECORD SYSTEM\n");
  printf("1 - add new record\n");
  printf("2 - update record\n");
  printf("3 - delete record\n");
  printf("4 - print all records\n");
  printf("5 - save as txt file\n");
  printf("6 - end program\n\n");
  printf("ENTER A CHOICE => ");

  int i;
  FILE *file = fopen(FILENAME, "rb+");
  scanf("%d", &i);
  printf("\n");
  switch (i) {
    case 1:
      addRecord(file);
      break;
    case 2:
      updateRecord(file);
      break;
    case 3:
      deleteRecord(file);
      break;
     case 4:
       showRecords(file);
       break;
     case 5:
       convertTextFile(file);
       break;
    case 6:
      exit(0);
  }
  
    printf("\n*************************************************\n\n");
    fclose(file);
}


void addRecord(FILE *file){
  int id;
  printf("Enter id to create new employee record: ");
  scanf("%d", &id);
  
  if(id < 1 || id > 100){
    printf("%d is an incorrect id, operation is cancelled.\n", id);
    return;
  }

  employeeData emp;
  fseek(file, sizeof(emp) * (id - 1), SEEK_SET);
  fread(&emp, sizeof(emp), 1, file);
  if(emp.id != 0){
    printf("Employee with id #%d already exists, operation is cancelled\n", emp.id);
    return;
  }
  emp.id=id;
  printf("\nEmployee Name: ");
  scanf("%s", emp.name);
  printf("\nDepartment: ");
  scanf("%s", emp.dept);
  printf("\nBirth Year: ");
  scanf("%d", &emp.birthYear);
  printf("\nSalary: ");
  scanf("%d", &emp.salary);

  fseek(file, -sizeof(emp), SEEK_CUR);
  fwrite(&emp, sizeof(emp), 1, file);
  printf("Employee with id #%d is recorded\n", emp.id);
}


void showRecords(FILE* file){
  employeeData emp;
  int oneRowFound=0;
  for(int i=0; i<100; i++){
    fread(&emp, sizeof(emp),1,file);
    if(emp.id != 0){
      if(oneRowFound==0){ 
        printEmployeeColumns(stdout);
        oneRowFound=1;
      }
      printEmployee(stdout, emp);
    }
  }
  if(oneRowFound==0){
    printf("There are no records to print, operation is cancelled\n");
  }
}

void convertTextFile(FILE* file){
  FILE* textFile = fopen("employee.txt", "w");
  int oneRowFound=0;
  employeeData emp;
  for(int i=0; i<100; i++){
    fread(&emp, sizeof(emp),1, file);
    if(emp.id != 0){
      if(oneRowFound==0){
        printEmployeeColumns(textFile);
        oneRowFound=1;
      }
      printEmployee(textFile, emp);
    }
  }
  fclose(textFile);
  if(oneRowFound)
    printf("\nemployee.txt file is ready!\n");
  else
    printf("There are no recrods to save, operation is cancelled\n");
}

void updateRecord(FILE* file){
  int id;
  printf("Enter id to update amount of employee's salary: ");
  scanf("%d", &id);
  
  if(id < 1 || id > 100){
    printf("%d is an incorrect id, operation is cancelled.\n", id);
    return;
  }

  employeeData emp;
  fseek(file, sizeof(emp) * (id - 1), SEEK_SET);
  fread(&emp, sizeof(emp), 1, file);
  if(emp.id == 0){
    printf("Employee with id #%d doesn't exists, operation is cancelled\n", id);
    return;
  }

  printEmployeeColumns(stdout);
  printEmployee(stdout, emp);
  printf("\nEnter the percentage of increase for salary: ");
  int perc;
  scanf("%d", &perc);
  emp.salary += emp.salary * perc * 0.01;
  fseek(file, -sizeof(emp.salary), SEEK_CUR);
  fwrite(&emp.salary, sizeof(emp.salary), 1, file);
  printEmployeeColumns(stdout);
  printEmployee(stdout, emp);
}

void deleteRecord(FILE* file){
  int id;
  printf("Enter id to remove employee record: ");
  scanf("%d", &id);
  
  if(id < 1 || id > 100){
    printf("%d is an incorrect id, operation is cancelled.\n", id);
    return;
  }
  employeeData emp;

  fseek(file, sizeof(employeeData) * (id - 1), SEEK_SET);
  fread(&emp.id, sizeof(emp.id), 1, file);
  fseek(file, -sizeof(emp.id), SEEK_CUR);
  if(emp.id == 0){
    printf("Employee with id #%d doesn't exists, operation is cancelled\n", id);
    return;
  }
  
  memset(&emp, 0, sizeof(emp));
  fwrite(&emp, sizeof(emp), 1, file);
  printf("Employee record with id #%d is removed\n", id);
}

void printEmployeeColumns(FILE* file){
  fprintf(file, "%-8s %-20s %-20s %-15s %-15s\n", "ID NO","Employee Name","Department","Birth Year","Salary");
}
void printEmployee(FILE* file, employeeData emp){
  fprintf(file, "%-8d %-20s %-20s %-15d %-15d\n",emp.id, emp.name, emp.dept, emp.birthYear, emp.salary);
}