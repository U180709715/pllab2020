#include <stdio.h>
#include <stdbool.h>
int nrDigits(int num)
{
    int ans = num / 10;
    return ans > 0 ? 1 + nrDigits(ans) : 1;
}

int GCD(int first, int second)
{
    static int num = 0;
    if (num == 0)
    {
        num = first;
    }

    if (first % num == 0 && second % num == 0)
        return num;
    else
    {
        num--;
        return GCD(first, second);
    }
}

bool prime(int num)
{
    if (num < 2)
        return false;
    static int cur = 2;
    if (cur >= num)
        return true;
    if (num % cur == 0)
        return false;
    cur++;
    return prime(num);
}

int main()
{
    printf("%d\n", nrDigits(533060));
    printf("%d\n", GCD(8, 20));
    printf("%s\n", prime(6) ? "true" : "false");
}
