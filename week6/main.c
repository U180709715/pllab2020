#include <stdio.h>
#include <math.h>
#include <stdbool.h>

void printArray (const int arr[], const int size) {
  for (int i = 0; i < size; i++)
    printf ("%d ", arr[i]);
  printf ("\n");
}

void swap (int *p, int *k) {
  *p = *p + *k;
  *k = *p - *k;
  *p = *p - *k;
}

bool stoogeSort (int arr[], const int low, const int high)
{
  //printf("stoogeSort: %d,%d\n", low, high);
  //getchar();
  if (low >= high)
    return true;
  if (arr[low] > arr[high])
    swap (arr+low, arr+high);
  int length = high - low + 1;
  if (length >= 3) {
      int lastElementInFirstTwoThirds = low + floor (length * 2.0 / 3.0) - 1;
      int firstElementInLastTwoThirds = low + ceil (length / 3.0);

      stoogeSort (arr, low, lastElementInFirstTwoThirds);
      //printf("passed 1\n");
      stoogeSort (arr, firstElementInLastTwoThirds, high);
      //printf("passed 2\n");
      stoogeSort (arr, low, lastElementInFirstTwoThirds);
      //printf("passed 3\n");
  }
  return true;
}

int main () {
  int size;
  printf("Enter array size: ");
  scanf("%d", &size);
  int arr[size];
  printf("Enter %d elements: ", size);
  for(int i=0; i<size; i++)
    scanf("%d", arr+i);
  stoogeSort (arr, 0, size-1);
  printArray (arr, size);
  return 0;
}