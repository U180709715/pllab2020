#include "CipherMethods.h"

int isSpecial(char);

char * encryptCaesar(const char * original){
  int len = strlen(original);
  char * encrypted = malloc((len+1) * sizeof(char));
  for(int i=0; i < len; i++)
    encrypted[i]= isSpecial(original[i]) ? original[i] : (encrypted[i] = original[i] + CAESAR_SHIFT) > 'z' ? encrypted[i] - 'z' + 'a' - 1 : encrypted[i] < 'a' && encrypted[i] > 'Z' ? encrypted[i] - 'Z'+'A' - 1 : encrypted[i] < 'A' && encrypted[i] > '9' ? encrypted[i] - '9' + '0' -1 : encrypted[i];
  encrypted[len]='\0';
  return encrypted;
}

char * decryptCaesar(const char * original){
  int len = strlen(original);
  char * decrypted = malloc((len+1) * sizeof(char));
  for(int i=0; i < len; i++)
    decrypted[i] = isSpecial(original[i]) ? original[i] : (decrypted[i] = original[i] - CAESAR_SHIFT) < '0' ? decrypted[i] + '9' - '0' + 1 : decrypted[i]> '9' && decrypted[i] < 'A' ? decrypted[i] + 'Z' - 'A' + 1 : decrypted[i] > 'Z' && decrypted[i] < 'a' ? decrypted[i] + 'z' - 'a' + 1 : decrypted[i];
  decrypted[len]='\0';
  return decrypted;
}



int isSpecial(char c){
  for(int i=0; i < sizeof(CIPHER_SPECIAL_CHARS) -1; i++)
    if(c==CIPHER_SPECIAL_CHARS[i])
      return 1;
  return 0;
}