#define TRACE_ORG_DEC
#include "CipherMethods.h"
#include <stdio.h>
void clearScreen();
int main(){
  char text[500];
  char passcode[9] = "ceng2002";

  printf("** You are about to enter a very secret cryptography service called CENG 2002 C-Secret Coded System **\n\n");
  printf("Enter your text:\n\n");

  fgets(text, 500, stdin);
  strtok(text, "\n");
  clearScreen();
  char * encrypted = encryptCaesar(text);
  printf("In order to see the encrypted message, enter your passcode:\n");
  int tries=0;
  for(char c[80], res=scanf("%s",c); strcmp(passcode, c)!=0 && tries<3; tries++, tries < 3? printf("Wrong passcode. Enter again:\n"),scanf("%s",c): 0);
  if(tries == 3){
    printf("Number of allowed attempts has been reached without successful entry!\nYour IP has been blacklisted by us. Good luck!! At least until your dynamic IP changes by tomorrow :)\n\n");
    return 0;
  }

  printf("Cipher: %s\n\n", encrypted);
  printf("Would you like to convert the cipher to original text (Y/N)?\n");
  char c, *decrypted;
  for(scanf("\n%c", &c); c!='y' && c!='Y' && c!='n' && c!='N'; printf("Please answer with Y or N.\n"),scanf("\n%c", &c));
  if(c=='Y' || c=='y')
    printf("%s\n\n", decrypted=decryptCaesar(encrypted));

#ifdef TRACE_ORG_DEC
  printf(strcmp(text, decrypted) == 0 ? "Original text and the decrypted text match." : "There is something wrong. Original text and the decrypted text do not match.");
  printf("\n\n");
#endif

}

void clearScreen() {
#ifdef WINDOWS
  system("cls");
#else
  system ("clear");
#endif
}