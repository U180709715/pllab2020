#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int scanLine(char* string);

int main()
{
    char string[80];
    printf("Enter the source string:\n> ");
    fgets(string, 80, stdin);
    string[strlen(string) -1]='\0';
    while (1){
        printf("Enter D(Delete), I(Insert), F(Find) or Q(Quit)> ");
        char cmd;
        scanf(" %c", &cmd);
        switch(cmd){
            case 'd':
            case 'D':{
                printf("String to delete> ");
                char str[80];
                int size=scanLine(str);
                
                for(int i=0; i<80; i++){
                    if(!strncmp(string+i, str, size)){
                        strcpy(string+i, string+i+size);
                    }
                }
                break;
            }

            case 'f':
            case 'F': {
                printf("String to find> ");
                char str[80];
                int size=scanLine(str);
                for(int i=0; i<80; i++){
                    if(!strncmp(string+i, str, size)){
                        printf("'%s' found at position %d\n", str, i);
                    }
                }
                break;
            }
            
            case 'i':
            case 'I': {
                printf("String to insert> ");
                char str[80];
                int size=scanLine(str);
                printf("Position of insertion> ");
                int pos;
                scanf("%d", &pos);
                int tempSize=80-pos;
                char temp[tempSize];
                strncpy(temp, string+pos, tempSize);
                strncpy(string+pos, str, size);
                strncpy(string+pos+size, temp, tempSize-size);
                break;
            }
            
            case 'q':
            case 'Q':
              printf("String after editing: %s\n", string);
              return 0;
                
            default:
             printf("\nERROR: %c doesn't match any of the options.\n", cmd);
        }
        printf("New source: %s\n\n", string);
    }
}


int scanLine(char* string) {
  char str[80];
  scanf("\n");
  fgets(str, 80, stdin);
  int size=strlen(str);
  str[size-1]='\0';
  strcpy(string, str);
  return size-1;
}